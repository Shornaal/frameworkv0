<?php
class UserController
{
	static public function executeLogon()
	{
		if (!isset($_SESSION['auth']) || !$_SESSION['auth'])
			return (html('user/logon.html.php'));
		redirect_to('/');
	}

	static public function executeSubscribeForm()
	{
		return (html('user/_form.html.php'));
	}

	static public function executeAdd()
	{
		if (isset($_POST['user']) && !empty($_POST['user']))
		{
			if ($manager = option("managers")->getManagerOf('user'))
			{
				if (!($manager->getUserByName($_POST['username'])))
				{
					$_POST['user']['password_hash'] = md5($_POST['user']['password']);
					$_POST['user']['groups_id'] = 0;
					if ($id = $manager->addUser($_POST['user']))
						self::executeAuth();
				}
				else
					halt(SERVER_ERROR, "Ce nom d'utilisateur existe deja");
			}
			else
				halt(SERVER_ERROR, "Impossible de trouver le manager user.");
		}
		return (html("Do nothing"));
	}

	static public function executeAuth()
	{
		if (isset($_POST['user']) && !empty($_POST['user']))
		{
			if ($manager = option("managers")->getManagerOf('user'))
			{
				if ($user = $manager->getUserByName($_POST['user']['username']))
				{
					if (md5($_POST['user']['password']) == $user[0]['password_hash'])
					{
						$authUser = new User();
						foreach ($user[0] as $key => $value)
							$authUser->setAttribute($key, $value);
						$authUser->setAuthenticated(true);
						echo $_SESSION['auth'];
						redirect_to('/');
					}
					// Ajouter data-validation
					halt(SERVER_ERROR, "Le mot de passe est incorrect");
				}
				else
					// Ajouter data-validation
					halt(NOT_FOUND, "Cette utilisateur n'existe pas");
			}
		}
	}

	static public function executeLogout()
	{
		if (isset($_SESSION['auth']) && !empty($_SESSION['auth']) && ($_SESSION['auth'] == true))
		{
			$_SESSION['auth'] = false;
			session_destroy();
		}
		redirect_to('/');
	}
}
?>
