<?php
class NewsController
{
	static public function executeIndex()
	{
		$newsPerPage = option('news_per_page');
		set('section_title', "Derniers post:");	
		if ($manager = option("managers")->getManagerOf('news'))
		{
			$listNews = $manager->getNews(0, $newsPerPage);
			if (!empty($listNews) && isset($listNews))
				set('listNews', $listNews);
			else
				halt(NOT_FOUND, "Il n'y a aucune news a afficher dans la base de donne.");
		}	
		return (html('news/show.html.php'));
	}

	static public function executeGetById()
	{
		$id = params('id');
		if (!is_numeric($id))
			halt(NOT_FOUND, "Cette news n'existe pas");
		set('section_title', "Sweg");
		if ($manager = option("managers")->getManagerOf('news'))
		{
			$news[] = $manager->getNewsById($id);
			if (!empty($news[0]) && isset($news[0]))
				set('listNews', $news);
			else
				halt(NOT_FOUND, "Cette news n'existe pas.");
		}
		return (html('news/show.html.php'));
	}

	static public function executeNew()
	{
		return (html('news/new.html.php'));
	}

	static public function executeAdd()
	{
		if ($manager = option("managers")->getManagerOf('news'))
		{
			if (isset($_SESSION['auth']) && $_SESSION['auth'])
			{
				$_POST['news']['author'] = $_SESSION['id'];
				if ($id = $manager->createNews($_POST['news']))
					redirect_to('news', $id);
			else
				halt(SERVER_ERROR, "Une erreur est survenue pendant la creation de la news.");
			}
		}
	}

	static public function executeUpdate()
	{
		$id = params('id');
		if ($manager = option("manager")->getManagerOf('news'))
		{
			if ($manager->updateNews($id, $_POST['data']))
				redirect_to('news', $id);
			else
				halt(SERVER_ERROR, "Une erreur est survenue pendant la modification
				du post " . $id);
		}
	}

	static public function executeDelete()
	{
		$id = params('id');
		if ($manager = option("manager")->getManagerOf('news'))
		{
			if ($manager->deleteNews($id))
				redirect_to('news');
			else
				halt(SERVER_ERROR, "Une erreur est survenue pendant la suppression
				de la news." . $id);
		}
	}
}
?>
