<?php
class PDONewsManager extends NewsManager
{
	public function getNews($start = -1, $limit = -1)
	{
		$request = "SELECT * FROM `News`";
		if ($limit > -1)
			$request .= " LIMIT " . $limit;
		if ($start > -1)
			$request .= " OFFSET " . $start;
		foreach ($this->_dao->query($request, PDO::FETCH_ASSOC) as $news)
		{
			$listNews[] = $news;
		}
		return ($listNews);
	}

	public function getNewsById($id)
	{
		if ($id < 0)
			throw new InvalidArgumentException("Index out of range on 
			NewsManager.");
		foreach($this->_dao->query("SELECT * FROM `News` WHERE id=" . $id, PDO::FETCH_ASSOC) as $news)
			return ($news);
		return (false);
	}

	public function createNews($data)
	{
		print_r($data);
		$request = "INSERT INTO `News` (`title`, `content`, `author`, `creation_date`, `update_date`) VALUES (:title, :content, :author, NOW(), NOW())";
		$stmt = $this->_dao->prepare($request);
		$stmt->bindValue(':title', htmlspecialchars($data['title']), PDO::PARAM_STR);
		$stmt->bindValue(':content', htmlspecialchars($data['content']), PDO::PARAM_STR);
		$stmt->bindValue(':author', htmlspecialchars($data['author']), PDO::PARAM_INT);
		if ($stmt->execute())
			return ($this->_dao->lastInsertId());
		return (false);
	}

	public function updateNews($id, $data)
	{
		$request = "UPDATE `News` SET title=:title, content=:content, update_date=DATETIME('NOW', 'localtime') WHERE id=:id";
		$stmt = $this->_dao->prepare($request);
		$stmt->bindValue(':id', $id, PDO::PARAM_INT);
		$stmt->bindValue(':title', $data['title'], PDO::PARAM_STR);
		$stmt->bindValue(':content', $data['content'], PDO::PARAM_STR);
		return ($stmt->execute());
	}

	public function deleteNews($id)
	{
		$request = "DELETE FROM `News` WHERE id=:id";
		$stmt = $this->_dao->prepare($request);
		$stmt->bindValue(':id', $id, PDO::PARAM_INT);
		return ($stmt->execute());
	}
}
?>
