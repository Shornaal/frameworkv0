<?php
abstract class NewsManager extends AbstractManager
{
	abstract public function getNews($start = -1, $limit = -1);
}
?>
