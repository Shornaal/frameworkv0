<?php
/**
 * Manager d'utilisateur abstrait. Permet de prototyper les fonctions
 * obligatoires pour tout manager d'utilisateur au sein de l'application.
 * @tiboitel.
 */
abstract class UserManager extends AbstractManager
{
	
	abstract public function addUser($user);
	abstract public function getUsers($start = -1, $limit = -1);
	abstract public function getUserById($id);
	abstract public function updateUser($id, $user);
	abstract public function deleteUser($id);
	abstract public function countUsers();
}
?>
