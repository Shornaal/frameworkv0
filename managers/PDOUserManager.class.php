<?php
class PDOUserManager extends UserManager
{
	public function addUser($user)
	{
		$request = "INSERT INTO `accounts` (`username`, `password_hash`, `email`, `groups_id`, `subscription_date`) VALUES (:username, :password_hash, :email, :groups_id, NOW())";
		$stmt = $this->_dao->prepare($request);
		$stmt->bindValue(":username", $user['username'], PDO::PARAM_STR);
		$stmt->bindValue(":password_hash", $user['password_hash'], PDO::PARAM_STR);
		$stmt->bindValue(":email", $user['email'], PDO::PARAM_STR);
		$stmt->bindValue(":groups_id", $user['groups_id'], PDO::PARAM_INT);
		if ($stmt->execute())
			return ($this->_dao->lastInsertId());
		return (false);
	}

	public function getUsers($start = -1, $limit = -1)
	{
		$request = "SELECT * FROM `accounts`";
		if ($limit > -1)
			$request .= " LIMIT " . $limit;
		if ($start > -1)
			$request .= " OFFSET " . $start;
		foreach ($this->_dao->query($request, PDO::FETCH_ASSOC) as $user)
		{
			$listUser[] = $user;
		}
		return ($listUser);
	}

	public function getUserById($id)
	{
		$request = "SELECT * FROM `accounts` WHERE id=:id";
		$stmt = $this->_dao->prepare($request);
		$stmt->bindParam(":id", $id, PDO::PARAM_INT);
		return ($stmt->execute());
	}
	
	public function getUserByName($name)
	{
		$request = "SELECT * FROM `accounts` WHERE username=(?)";
		$stmt = $this->_dao->prepare($request);
		$stmt->bindParam(1, $name, PDO::PARAM_STR);
		if ($stmt->execute())
			return ($stmt->fetchAll(PDO::FETCH_ASSOC));
		return (false);
	}

	public function updateUser($id, $user)
	{
		$request = "UPDATE `accouts` SET email=:email, groups_id=:groups_id WHERE id=:id;";
		$stmt = $this->_dao->prepare($request);
		$stmt->bindValue(':email', $user['email']);
		$stmt->bindValue(':groups_id', $user['groups_id']);
		return ($stmt->execute());
	
	}

	public function deleteUser($id)
	{
		$request = "DELETE FROM `accounts` WHERE id=:id";
		$stmt = $this->_dao->prepare($request);
		$stmt->bindValue(':id', $id, PDO::PARAM_INT);
		return ($stmt->execute());
	}

	public function countUsers()
	{
		return($this->dao->query("SELECT COUNT(*) FROM `accounts`"));
	}
}
?>
