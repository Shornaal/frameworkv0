<?php
class PDOFactory
{
	public static function getPDOConnection($host, $dbname, $user, $passwd = false)
	{
		if (!isset($host) || empty($host))
			throw new InvalidArgumentException("Le parametre host est invalide ou inexistant.");
		$db = new PDO('mysql:host=' . $host . ';dbname=' . $dbname, $user, $passwd);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return ($db);
	}
}
?>
