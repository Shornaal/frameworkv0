<?php
/**
 * 
 */
class News extends Entity
{
	
	/**
	 * 
	 */
	protected $author;
	protected $title;
	protected $content;
	protected $dateAdded;
	protected $dateUptade;

	const INVALID_AUTHOR = 1;
	const INVALID_TITLE = 2;
	const INVALID_CONTENT = 3;

	public function isValid()
	{
		return !(empty($this->author) || empty($this->title) ||
			empty($this->content));
	}

	public function setAuthor($author)
	{
		if (!is_string($author) || empty($author))
		{
			$this->errors[] = self::INVALID_AUTHOR;
		}
		$this->author = $author;
	}

	public function setTitle($title)
	{
		if (!is_string($title) || empty($title))
			$this->errors[] = self::INVALID_TITLE;
		$this->title = $title;
	}

	public function setContent($content)
	{
		if (!is_string($content) || empty($content))
			$this->errors[] = self::INVALID_CONTENT;
		$this->content = $content;
	}

	public function setDateAdded(DateTime $dateAdded)
	{
		$this->dateAdded = $dateAdded;
	}

	public function setDateUpdate(DateTime $dateUpdate)
	{
		$this->dateUpdate = $dateUpdate;
	}

	public function getAuthor()
	{
		return ($this->author);
	}

	public function getTitle()
	{
		return ($this->title);
	}

	public function getContent()
	{
		return ($this->content);
	}

	public function getDateAdded()
	{
		return ($this->dateAdded);
	}

	public function getDateUpdate()
	{
		return ($this->dateUpdate);
	}
}
?>
