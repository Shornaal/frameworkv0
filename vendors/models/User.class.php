<?php
class User
{
	public function getAttribute($attr)
	{
		return (isset($_SESSION[$attr]) ? $_SESSION[$attr] : null);
	}

	public function setAttribute($attr, $value)
	{
		$_SESSION[$attr] = $value;
	}

	public function hasFlash()
	{
		return (isset($_SESSION['flash']));
	}

	public function getFlash()
	{
		$flash = $_SESSION['flash'];
		unset($_SESSION['flash']);
		return ($flash);
	}

	public function setFlash()
	{
		$_SESSION['flash'] = $value;
	}

	public function isAuthenticated()
	{
		return (isset($_SESSION['auth']) && $_SESSION['auth'] === true);
	}

	public function setAuthenticated($authenticated = true)
	{
		if (!is_bool($authenticated))
		{
			throw new InvalidArgumentException('Value specificated at method
				User::setAuthenticated must be a boolean.');
		}
		$_SESSION['auth'] = $authenticated;
	}
}
?>
