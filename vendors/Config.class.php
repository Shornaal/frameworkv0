<?php
/**
 * 
 */
class Config
{
	
	/**
	 * 
	 */
	protected	$filePath;

	public function __construct($filePath)
	{
		if (!is_string($filePath) || !isset($filePath))
			throw new InvalidArgumentException("Argument given to constructor
			of Config object is invalid");
		$this->filePath = $filePath;
	}

	public function	loadConfig()
	{
		if (($options = simplexml_load_file($this->filePath)))
		{
			foreach($options->option as $object)
			{
				option("" . $object->attributes()["id"],
				   	"" . $object->attributes()[1]);
			}
		}
		else
			throw new RuntimeException("Application unable to load configuration
			file.");
	}
}
?>
