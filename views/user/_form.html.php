<div id="row">
	<div class="col-lg-8 col-lf-offset-2 col-md-10 col-md-offset-1">
	<h3>Inscription</h3>
	<form name="sentUser" id="sentUserForm" method="POST" action="<?php echo url_for('user', 'subscribe'); ?>" novalide>
		<div class="row control-group">
			<div class="form-group col-xs-12 floating-label-form-group controls">
				<label>Nom de compte</label>
				<input type="text" class="form-control" placeholder="Nom d'utilisateur" id="username" name="user[username]" required-data-validation-message="Veuillez entrer un nom d'utilisateur.">
				<p class="help-block text-danger"></p>
			</div>
		</div>
		<div class="row control-group">
			<div class="form-group col-xs-12 floating-label-form-group controls">
				<label>Mot de passe</label>
				<input type="password" class="form-control" placeholder="Mot de passe" id="password" name="user[password]" required-data-validation-message="Veuillez entrer un nom d'utilisateur.">
				<p class="help-block text-danger"></p>
			</div>
		</div>
		<div class="row control-group">
			<div class="form-group col-xs-12 floating-label-form-group controls">
				<label>Email</label>
				<input type="text" class="form-control" placeholder="Email" id="email" name="user[email]" required-data-validation-message="Veuillez entrer un nom d'utilisateur.">
				<p class="help-block text-danger"></p>
			</div>
		</div>
		<div id="success">
		</div>
		<div class="row">
			<div class="form-group col-xs-12">
				<button type="submit" class="btn btn-default">Envoyer</button>
			</div>
		</div>
	</form>
	</div>
</div>

<hr>
