<div class="row">
	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
	<h3>Authentification</h3>
	<form name="sentLogon" id="sentLogonForm"
		action="<?php echo url_for('user', 'logon');?>" method="POST">
		<div class="row control-group">
			<div class="form-group col-xs-12 floating-label-form-group controls">
				<label>Nom d'utilisateur</label>
				<input type="text" class="form-control" placeholder="Nom d'utilisateur" 
					id="username" name="user[username]"
					required data-validation-required-message=
					"Veuillez saisir votre nom d'utilisateur"/>
				<p class="help-block text-danger"></p>
			</div>
		</div>
		<div class="row control-group">
			<div class="form-group col-xs-12 floating-label-form-group controls">
				<label>Mot de passe</label>
				<input type="password" class="form-control" placeholder="Mot de passe"
					id="password" name="user[password]"
					required data-validation-required-message=
					"Veuillez saisir votre mot de passe"/>
				<p class="help-block text-danger"></p>
			</div>
		</div>
		<div id="success"></div>
		<div class="row">
			<div class="form-group col-xs-12">
				<button type="submit" class="btn btn-default">Se connecter</button>
				<p class="text-right"><a href="<?php echo url_for('user', 'subscribe'); ?>">S'inscrire</a></p>
			</div>
		</div>
	</form>
	</div>
</div>
