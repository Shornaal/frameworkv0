<div class="row">
	<div class="col-lg-8 col-lg-offset-2 col-md-10 cold-md-offset-1">
		<?php foreach($listNews as $news)
		{
		?>
		<div class="post-preview">
			<a href="<?php echo url_for('news', $news['id']); ?>">
			<h2 class="post-title">
				<?php echo $news['title']; ?>
			</h2>
			</a>
			<p class="post-meta">Ecrit par <a href="<?php echo url_for('user', $news['author']); ?>"><?php echo $news['author']; ?></a> le <?php echo $news['creation_date']; ?></p>
		</div>
		<div class="post-content">
			<p><?php echo $news['content']; ?></p>
		</div>
		<hr>
		<?php } ?>
	</div>
</div>
