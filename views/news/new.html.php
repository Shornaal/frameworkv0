<div class="row">
	<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
	<h3>Ajouter un article</h3>
	<form name="sentNews" id="sentNewsForm" method="POST" action="<?php echo url_for('news', 'add'); ?>" novalide>
		<div class="row control-group">
			<div class="form-group col-xs-12 floating-label-form-group controls">
				<label>Title</label>
				<input type="text" class="form-control" name="news[title]" placeholder="Titre" id="data[title]"
				required data-validation-required-message="Veuillez entrer un titre">
				<p class="help-block text-danger"></p>
			</div>
		</div>
		<div class="row control-group">
			<div class="form-group col-xs-12 floating-label-form-group controls">
				<label>Contenue:</label>
				<textarea rows="5" cols="50" class="form-control" placeholder="Corps du message" id="data[content]"
				required data-validation-required-message="Veuillez le corps du message" name="news[content]"></textarea>
				<p class="help-block text-danger"></p>
			</div>
			<div id="success"></div>
			<div class="row">
				<div class="form-group col-xs-12">
					<button type="submit" class="btn btn-default">Envoyer</button>
				</div>
			</div>
		</div>
	</form>
	</div>
</div>

<hr>
