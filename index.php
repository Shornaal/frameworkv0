<?php
require_once('vendors/limonade.php');

spl_autoload_register(function($name)
	{
		$dirs= array('managers/',
					'controllers/',
					'vendors/',
					'vendors/models/');
		foreach ($dirs as $dir)
		{
			if (file_exists($dir . $name . '.class.php'))
			{
				require_once($dir . $name . '.class.php');
				return ;
			}
		}
	});

session_start();

function configure()
{
	option('base_uri', '/');
	$config = new Config("vendors/configuration.xml");
	$config->loadConfig();
	option('managers', new Managers( "PDO",
		PDOFactory::getPDOConnection(option('database_ip'),
	   	option('database_name'),
		option('database_user'),
	  	option('database_passwd'))));
	setlocale(LC_TIME, "fr_FR");
}

function before()
{
	layout('layouts/default.html.php');
}

/* [NEWS] REST map */
dispatch('/', array('NewsController','executeIndex'));
dispatch('/news/', array('NewsController', 'executeIndex'));
dispatch_post('/news/add', array('NewsController', 'executeAdd'));
dispatch('/news/add', array('NewsController', 'executeNew'));
dispatch('/news/:id', array('NewsController', 'executeGetById'));
dispatch_delete('/news/:id', array('NewsController', 'executeDelete'));

/* [USER] REST map */
dispatch_post('/user/subscribe', array('UserController', 'executeAdd'));
dispatch('/user/subscribe', array('UserController', 'executeSubscribeForm'));
dispatch_post('/user/logon', array('UserController', 'executeAuth'));
dispatch('/user/logon', array('UserController', 'executeLogon'));
dispatch('/user/logout', array('UserController', 'executeLogout'));
run();
?>
